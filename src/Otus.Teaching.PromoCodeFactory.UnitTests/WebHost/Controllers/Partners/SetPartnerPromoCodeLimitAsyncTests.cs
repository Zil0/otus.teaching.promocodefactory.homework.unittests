﻿using System;
using System.Linq;
using AutoFixture;
using AutoFixture.AutoMoq;
using FluentAssertions;
using Microsoft.AspNetCore.Mvc;
using Moq;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Builders;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Controllers;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using Otus.Teaching.PromoCodeFactory.WebHost.Models.Builders;
using Xunit;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.WebHost.Controllers.Partners
{
    public class SetPartnerPromoCodeLimitAsyncTests
    {
        private readonly Mock<IRepository<Partner>> _partnersRepositoryMock;
        private readonly PartnersController _partnersController;
        private readonly int _randomInt;

        public SetPartnerPromoCodeLimitAsyncTests()
        {
            var fixture = new Fixture().Customize(new AutoMoqCustomization());
            _partnersRepositoryMock = fixture.Freeze<Mock<IRepository<Partner>>>();
            _partnersController = fixture.Build<PartnersController>().OmitAutoProperties().Create();

            _randomInt = new Random().Next(1, 100);
        }

        /// <summary>
        /// Если партнер не найден, то также нужно выдать ошибку 404
        /// </summary>
        /// <returns></returns>
        [Fact]
        public async System.Threading.Tasks.Task PartnersController_SetPartnerPromoCodeLimitAsync_ReturnNotFoundAsync()
        {
            //Arrange
            var partner = new PartnerBuilder().Build();
            _partnersRepositoryMock.Setup(x => x.GetByIdAsync(partner.Id)).ReturnsAsync(() => null);

            //Act
            var result =
                await _partnersController.SetPartnerPromoCodeLimitAsync(partner.Id,
                    new SetPartnerPromoCodeLimitRequest());
            //Assert
            result.Should().BeAssignableTo<NotFoundResult>();
        }

        /// <summary>
        /// Если партнер заблокирован, то есть поле IsActive=false в классе Partner, то также нужно выдать ошибку 400
        /// </summary>
        [Fact]
        public async void PartnersController_SetPartnerPromoCodeLimitAsync_ReturnBadRequest()
        {
            //Arrange
            var partner = new PartnerBuilder().AddIsActive(false).Build();
            _partnersRepositoryMock.Setup(x => x.GetByIdAsync(partner.Id)).ReturnsAsync(() => partner);

            //Act
            var result =
                await _partnersController.SetPartnerPromoCodeLimitAsync(partner.Id,
                    new SetPartnerPromoCodeLimitRequest());
            //Assert
            result.Should().BeAssignableTo<BadRequestObjectResult>();
        }

        /// <summary>
        /// Если партнеру выставляется лимит, то мы должны обнулить количество промокодов, которые партнер выдал NumberIssuedPromoCodes, если лимит закончился, то количество не обнуляется
        /// </summary>
        [Theory]
        [InlineData(1)]
        [InlineData(20)]
        public async void PartnersController_SetPartnerPromoCodeLimitAsync_NumberIssuedPromoCodesEqualZero(int codes)
        {
            //Arrange
            var partner = new PartnerBuilder()
                .AddIsActive(true)
                .AddNumberIssuedPromoCodes(codes)
                .AddPartnerLimits(codes)
                .Build();

            var request = new SetPartnerPromoCodeLimitRequestBuilder()
                .AddLimit(new Random().Next(1, 100))
                .Build();
            _partnersRepositoryMock.Setup(x => x.GetByIdAsync(partner.Id)).ReturnsAsync(() => partner);

            //Act
            await _partnersController.SetPartnerPromoCodeLimitAsync(partner.Id, request);

            //Assert
            partner.NumberIssuedPromoCodes.Should().Be(0);
        }

        [Theory]
        [InlineData(1)]
        [InlineData(20)]
        public async void PartnersController_SetPartnerPromoCodeLimitAsync_NumberIssuedPromoCodesNotChanged(int codes)
        {
            //Arrange
            var partner = new PartnerBuilder()
                .AddIsActive(true)
                .AddNumberIssuedPromoCodes(codes)
                .Build();

            var request = new SetPartnerPromoCodeLimitRequestBuilder()
                .AddLimit(codes)
                .Build();

            _partnersRepositoryMock.Setup(x => x.GetByIdAsync(partner.Id)).ReturnsAsync(() => partner);

            //Act
            await _partnersController.SetPartnerPromoCodeLimitAsync(partner.Id, request);

            //Assert
            partner.NumberIssuedPromoCodes.Should().Be(codes);

        }

        /// <summary>
        /// При установке лимита нужно отключить предыдущий лимит
        /// </summary>
        [Theory]
        [InlineData(1)]
        [InlineData(20)]
        public async void PartnersController_SetPartnerPromoCodeLimitAsync_DisableOldLimit(int codes)
        {
            //Arrange
            var partner = new PartnerBuilder()
                .AddIsActive(true)
                .AddNumberIssuedPromoCodes(codes)
                .AddPartnerLimits(codes)
                .Build();

            var limit = partner.PartnerLimits.First();

            var request = new SetPartnerPromoCodeLimitRequestBuilder()
                .AddLimit(codes)
                .Build();

            _partnersRepositoryMock.Setup(x => x.GetByIdAsync(partner.Id)).ReturnsAsync(() => partner);

            //Act
            await _partnersController.SetPartnerPromoCodeLimitAsync(partner.Id, request);
            
            //Assert
            limit.CancelDate.Should().HaveValue();
        }

        /// <summary>
        /// Лимит должен быть больше 0
        /// </summary>
        [Theory]
        [InlineData(0)]
        [InlineData(-1)]
        public async void PartnersController_SetPartnerPromoCodeLimitAsync_LimitShouldBeMoreZero(int limit)
        {
            //Arrange
            var partner = new PartnerBuilder()
                .AddIsActive(true)
                .AddNumberIssuedPromoCodes(1)
                .AddPartnerLimits(1)
                .Build();

            var request = new SetPartnerPromoCodeLimitRequestBuilder()
                .AddLimit(limit)
                .Build();

            _partnersRepositoryMock.Setup(x => x.GetByIdAsync(partner.Id)).ReturnsAsync(() => partner);
            
            //Act
            var result =
                await _partnersController.SetPartnerPromoCodeLimitAsync(partner.Id,
                    new SetPartnerPromoCodeLimitRequest());
            //Assert
            result.Should().BeAssignableTo<BadRequestObjectResult>();
        }

        [Fact]
        public async void PartnersController_SetPartnerPromoCodeLimitAsync_RepositoryShouldCallUpdateAsyncOnce()
        {
            //Arrange
            var partner = new PartnerBuilder()
                .AddIsActive(true)
                .AddNumberIssuedPromoCodes(1)
                .AddPartnerLimits(1)
                .Build();

            var request = new SetPartnerPromoCodeLimitRequestBuilder()
                .AddLimit(10)
                .Build();
            
            _partnersRepositoryMock.Setup(x => x.GetByIdAsync(partner.Id)).ReturnsAsync(() => partner);
            
            //Act
            await _partnersController.SetPartnerPromoCodeLimitAsync(partner.Id, request);
            
            //Assert
            _partnersRepositoryMock.Verify(x=>x.UpdateAsync(partner),Times.Once);

        }

    }
}